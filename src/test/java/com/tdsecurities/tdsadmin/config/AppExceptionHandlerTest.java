package com.tdsecurities.tdsadmin.config;

import static org.junit.Assert.assertThat;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

public class AppExceptionHandlerTest {

    @Test
    public void testHandleException_responseStatusException() {

        ResponseStatusException responseStatusException = new ResponseStatusException(HttpStatus.BAD_REQUEST, "Just a bad request");

        AppExceptionHandler exceptionHandler = new AppExceptionHandler();
        ResponseEntity responseEntity = exceptionHandler.handleException(responseStatusException);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void testHandleNoHandlerFoundException() {

        Exception exception = new Exception("Something went wrong");

        AppExceptionHandler exceptionHandler = new AppExceptionHandler();
        ResponseEntity responseEntity = exceptionHandler.handleNoHandlerFoundException(exception);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.NOT_FOUND));
    }


}
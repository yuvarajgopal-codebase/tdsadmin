package com.tdsecurities.tdsadmin.testutil;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ResponseStatusExceptionMatcher extends TypeSafeMatcher<ResponseStatusException> {

    public static ResponseStatusExceptionMatcher hasStatus(HttpStatus httpStatus) {
        return new ResponseStatusExceptionMatcher(httpStatus);
    }

    private HttpStatus foundHttpStatus;
    private HttpStatus expectedHttpStatus;

    private ResponseStatusExceptionMatcher(HttpStatus expectedHttpStatus) {
        this.expectedHttpStatus = expectedHttpStatus;
    }

    @Override
    protected boolean matchesSafely(ResponseStatusException exception) {
        this.foundHttpStatus = exception.getStatus();
        return foundHttpStatus.equals(expectedHttpStatus);
    }

    @Override
    public void describeTo(Description description) {
        description.appendValue(foundHttpStatus.value()).appendText(" was not found instead of ").appendValue(expectedHttpStatus);

    }

}
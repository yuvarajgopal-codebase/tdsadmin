package com.tdsecurities.tdsadmin.controller;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.tdsecurities.common.dto.PageEditDTO;
import com.tdsecurities.common.service.PageEditService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(PageEditController.class)
public class PageEditControllerWebTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PageEditService pageEditServiceMock;

    @Test
    public void testGetPageDataToEdit_Successful() throws Exception {

        PageEditDTO pageEditDto = new PageEditDTO();
        when(pageEditServiceMock.getPageToEdit(anyString(), anyString())).thenReturn(pageEditDto);

        this.mockMvc.perform(get("/api/v1/content/AboutUsLeadership/edit")).andExpect(status().isOk());
        verify(pageEditServiceMock, times(1)).getPageToEdit(anyString(), anyString());
    }


}
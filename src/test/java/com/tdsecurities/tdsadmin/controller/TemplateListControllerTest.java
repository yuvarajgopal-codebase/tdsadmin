package com.tdsecurities.tdsadmin.controller;

import static org.mockito.Mockito.when;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import com.tdsecurities.common.dto.TemplateInfoDTO;
import com.tdsecurities.common.service.TemplateService;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.mockito.Mockito;

public class TemplateListControllerTest {

    @Test
    public void testGetTemplates_Successful() throws Exception {

        TemplateService templateServiceMock = Mockito.mock(TemplateService.class);

        List<TemplateInfoDTO> templateDtoList = new ArrayList<TemplateInfoDTO>();
        when(templateServiceMock.getTemplates()).thenReturn(templateDtoList);

        TemplateListController templateListController = new TemplateListController();
        templateListController.templateService = templateServiceMock;
        
        ResponseEntity responseEntity = templateListController.getTemplates();
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
    }

}
package com.tdsecurities.tdsadmin.controller;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;

import com.tdsecurities.common.dto.ImageDTO;
import com.tdsecurities.common.dto.PageInfoDTO;
import com.tdsecurities.common.service.ImageService;
import com.tdsecurities.common.service.PageService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(ImageController.class)
public class ImageControllerWebTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ImageService imageServiceMock;

    @Test
    public void testGetImage_Successful() throws Exception {

        ImageDTO imageDTO = new ImageDTO();
        imageDTO.setId("LeadershipBanner");
        imageDTO.setVersion(1L);
        imageDTO.setImageData("DummyImage".getBytes());
        when(imageServiceMock.getDefaultImage(anyString(), anyLong())).thenReturn(imageDTO);

        this.mockMvc.perform(get("/api/v1/image/LeadershipBanner/1")).andExpect(status().isOk());
        verify(imageServiceMock, times(1)).getDefaultImage(anyString(), anyLong());
    }

}
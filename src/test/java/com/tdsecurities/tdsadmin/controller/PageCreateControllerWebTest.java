package com.tdsecurities.tdsadmin.controller;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.google.gson.Gson;
import com.tdsecurities.common.dto.PageInfoDTO;
import com.tdsecurities.common.service.PageService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(PageCreateController.class)
public class PageCreateControllerWebTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PageService pageServiceMock;

    @Test
    public void testPostCreatePage_Successful() throws Exception {

        PageInfoDTO pageInfoDto = new PageInfoDTO("AboutUsLeadership", 1, "Ledership", 1,
            "Notes", null);

        Gson gson = new Gson();
        String pageInfoJson = gson.toJson(pageInfoDto);

        when(pageServiceMock.createPage(anyString(), anyString(), anyString())).thenReturn(pageInfoDto);

        this.mockMvc.perform(post("/api/v1/content/AboutUsLeadership")
            .contentType(MediaType.APPLICATION_JSON)
            .content(pageInfoJson)
            .accept(MediaType.APPLICATION_JSON)).andExpect(status().isCreated());
        verify(pageServiceMock, times(1)).createPage(anyString(), anyString(), anyString());
    }

}
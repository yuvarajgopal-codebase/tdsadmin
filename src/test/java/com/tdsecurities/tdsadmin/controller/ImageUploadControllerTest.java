package com.tdsecurities.tdsadmin.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

import com.tdsecurities.common.data.ImageSize;
import com.tdsecurities.common.dto.ImageMetadataDTO;
import com.tdsecurities.common.service.ImageService;
import com.tdsecurities.tdsadmin.testutil.ResponseStatusExceptionMatcher;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.mockito.Mockito;

public class ImageUploadControllerTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void testUploadSingleImage_Successful() throws Exception {


        MultipartFile multipartFile = new MockMultipartFile("Myfile", "Myfile.jpg", MediaType.IMAGE_JPEG_VALUE,
            "MyFile".getBytes());

        ImageMetadataDTO imageMetadataDTO = new ImageMetadataDTO();
        imageMetadataDTO.setId("Myfile");
        imageMetadataDTO.setVersion(1L);
        imageMetadataDTO.setDefaultSize(ImageSize.md);
        imageMetadataDTO.setMimeType(MediaType.IMAGE_JPEG_VALUE);

        ImageService imageServiceMock = Mockito.mock(ImageService.class);
        when(imageServiceMock.saveSingleImage(anyString(), anyString(), anyString(), any(byte[].class), anyString())).thenReturn(imageMetadataDTO);

        ImageUploadController imageUploadController = new ImageUploadController();
        imageUploadController.imageService = imageServiceMock;
        
        ResponseEntity responseEntity = imageUploadController.uploadSingleImage(multipartFile, "Myfile", "Notes for my image");
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.CREATED));
        assertThat(responseEntity.getBody().toString().contains("Myfile"), is(true));
    }

    @Test
    public void testUploadSingleImage_NullInput() throws Exception {

        exceptionRule.expect(ResponseStatusException.class);
        exceptionRule.expect(ResponseStatusExceptionMatcher.hasStatus(HttpStatus.BAD_REQUEST));

        ImageUploadController imageUploadController = new ImageUploadController();
        ResponseEntity responseEntity = imageUploadController.uploadSingleImage(null, "Myfile", "Notes for my image");

    }

    @Test
    public void testUploadSingleImage_InvalidContentType() throws Exception {

        exceptionRule.expect(ResponseStatusException.class);
        exceptionRule.expect(ResponseStatusExceptionMatcher.hasStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE));

        MultipartFile multipartFile = new MockMultipartFile("Myfile", "Myfile.jpg", MediaType.APPLICATION_JSON_VALUE,
            "MyFile".getBytes());
        ImageUploadController imageUploadController = new ImageUploadController();
        ResponseEntity responseEntity = imageUploadController.uploadSingleImage(multipartFile, "Myfile", "Notes for my image");

    }

    @Test
    public void testUploadSingleImage_ImageTooLarge() throws Exception {

        exceptionRule.expect(ResponseStatusException.class);
        exceptionRule.expect(ResponseStatusExceptionMatcher.hasStatus(HttpStatus.PAYLOAD_TOO_LARGE));

        MultipartFile multipartFile = Mockito.mock(MultipartFile.class);
        when(multipartFile.getName()).thenReturn("Myfile");
        when(multipartFile.getOriginalFilename()).thenReturn("Myfile.jp");
        when(multipartFile.getSize()).thenReturn(5000002L);
        when(multipartFile.getContentType()).thenReturn(MediaType.IMAGE_JPEG_VALUE);
        
        ImageUploadController imageUploadController = new ImageUploadController();
        ResponseEntity responseEntity = imageUploadController.uploadSingleImage(multipartFile, "Myfile", "Notes for my image");

    }

    @Test
    public void testUploadMultipleImages_Successful() throws Exception {

        MultipartFile multipartFile1 = new MockMultipartFile("Myfile1", "Myfile1.jpg", MediaType.IMAGE_JPEG_VALUE,
            "MyFile".getBytes());
        MultipartFile multipartFile2 = new MockMultipartFile("Myfile2", "Myfile2.jpg", MediaType.IMAGE_JPEG_VALUE,
            "MyFile".getBytes());
        MultipartFile multipartFile3 = new MockMultipartFile("Myfile3", "Myfile3.jpg", MediaType.IMAGE_JPEG_VALUE,
            "MyFile".getBytes());
        MultipartFile multipartFile4 = new MockMultipartFile("Myfile4", "Myfile4.jpg", MediaType.IMAGE_JPEG_VALUE,
            "MyFile".getBytes());

        ImageMetadataDTO imageMetadataDTO = new ImageMetadataDTO();
        imageMetadataDTO.setId("Myfile");
        imageMetadataDTO.setVersion(1L);
        imageMetadataDTO.setDefaultSize(ImageSize.md);
        imageMetadataDTO.setMimeType(MediaType.IMAGE_JPEG_VALUE);

        ImageService imageServiceMock = Mockito.mock(ImageService.class);
        when(imageServiceMock.saveMultiImage(anyString(), anyString(), any(ImageSize.class), anyString(),
            any(byte[].class), anyString(), any(byte[].class), anyString(), any(byte[].class), anyString(),
            any(byte[].class), anyString())).thenReturn(imageMetadataDTO);

        ImageUploadController imageUploadController = new ImageUploadController();
        imageUploadController.imageService = imageServiceMock;
        
        ResponseEntity responseEntity = imageUploadController.uploadMultipleImages(multipartFile1,
            multipartFile2, multipartFile3, multipartFile4, ImageSize.md, "Myfile", "Notes for my image");
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.CREATED));
        assertThat(responseEntity.getBody().toString().contains("Myfile"), is(true));
    }


}
package com.tdsecurities.tdsadmin.controller;

import static org.mockito.Mockito.when;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import com.tdsecurities.common.dto.PageInfoDTO;
import com.tdsecurities.common.service.PageService;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.mockito.Mockito;

public class PageListControllerTest {

    @Test
    public void testGetTemplates_Successful() throws Exception {

        PageService pageServiceMock = Mockito.mock(PageService.class);

        List<PageInfoDTO> pageInfoDtoList = new ArrayList<PageInfoDTO>();
        when(pageServiceMock.getPages()).thenReturn(pageInfoDtoList);

        PageListController pageListController = new PageListController();
        pageListController.pageService = pageServiceMock;
        
        ResponseEntity responseEntity = pageListController.getPages();
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
    }

}
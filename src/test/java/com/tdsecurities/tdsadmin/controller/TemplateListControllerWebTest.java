package com.tdsecurities.tdsadmin.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;

import com.tdsecurities.common.dto.TemplateInfoDTO;
import com.tdsecurities.common.service.TemplateService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(TemplateListController.class)
public class TemplateListControllerWebTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TemplateService templateServiceMock;


    @Test
    public void testGetTemplates_Successful() throws Exception {

        List<TemplateInfoDTO> templateDtoList = new ArrayList<TemplateInfoDTO>();
        when(templateServiceMock.getTemplates()).thenReturn(templateDtoList);

        this.mockMvc.perform(get("/api/v1/templates")).andExpect(status().isOk());
        verify(templateServiceMock, times(1)).getTemplates();
    }

    @Test
    public void testGetTemplates_Exception() throws Exception {
        when(templateServiceMock.getTemplates()).thenThrow(new RuntimeException("Error in TemplateService"));
        this.mockMvc.perform(get("/api/v1/templates")).andExpect(status().isInternalServerError());
    }


}
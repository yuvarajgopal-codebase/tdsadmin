package com.tdsecurities.tdsadmin.controller;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

import com.tdsecurities.common.dto.ImageDTO;
import com.tdsecurities.common.service.ImageService;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.mockito.Mockito;

public class ImageControllerTest {

    @Test
    public void testGetImage_Successful() throws Exception {

        ImageService imageServiceMock = Mockito.mock(ImageService.class);

        ImageDTO imageDTO = new ImageDTO();
        imageDTO.setId("LeadershipBanner");
        imageDTO.setVersion(1L);
        imageDTO.setImageData("DummyImage".getBytes());
        imageDTO.setMimeType("image/jpeg");
        when(imageServiceMock.getDefaultImage(anyString(), anyLong())).thenReturn(imageDTO);

        ImageController imageController = new ImageController();
        imageController.imageService = imageServiceMock;
        
        ResponseEntity responseEntity = imageController.getImage("Banner", 1L, null);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
        assertThat(responseEntity.getHeaders().get("Content-Type").contains("image/jpeg"), is(true));
    }

}
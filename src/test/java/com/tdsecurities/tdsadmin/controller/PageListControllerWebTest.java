package com.tdsecurities.tdsadmin.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;

import com.tdsecurities.common.dto.PageInfoDTO;
import com.tdsecurities.common.service.PageService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(PageListController.class)
public class PageListControllerWebTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PageService pageServiceMock;


    @Test
    public void testGetPages_Successful() throws Exception {

        List<PageInfoDTO> pageDtoList = new ArrayList<PageInfoDTO>();
        when(pageServiceMock.getPages()).thenReturn(pageDtoList);

        this.mockMvc.perform(get("/api/v1/content/pages")).andExpect(status().isOk());
        verify(pageServiceMock, times(1)).getPages();
    }

    @Test
    public void testGetPages_Exception() throws Exception {
        when(pageServiceMock.getPages()).thenThrow(new RuntimeException("Error in PageService"));
        this.mockMvc.perform(get("/api/v1/content/pages")).andExpect(status().isInternalServerError());
    }


}
package com.tdsecurities.tdsadmin.controller;

import java.io.IOException;

import com.tdsecurities.common.data.ImageSize;
import com.tdsecurities.common.dto.ImageMetadataDTO;
import com.tdsecurities.common.exception.ItemExistsException;
import com.tdsecurities.common.service.ImageService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

@Controller
@RequestMapping(path="/api/v1")
public class ImageUploadController {

    private static final Logger logger = LoggerFactory.getLogger(ImageUploadController.class);

    private static long MAX_IMAGE_UPLOAD_SIZE = 5000000;  // 5 MB

    @Autowired
    ImageService imageService;

    @PostMapping(path="/image-upload-single/{imageId}")
    public ResponseEntity<?>  uploadSingleImage(@RequestParam MultipartFile image,
        @PathVariable("imageId") String imageId, @RequestParam(required = false) String notes) {

        logger.info("Uploading new image {}", imageId);

        if (image == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No image to upload");
        }

        if (image.getSize() > MAX_IMAGE_UPLOAD_SIZE) {
            throw new ResponseStatusException(HttpStatus.PAYLOAD_TOO_LARGE, "Image too large");
        }

        if (!image.getContentType().equals(MediaType.IMAGE_JPEG_VALUE) &&
            !image.getContentType().equals(MediaType.IMAGE_GIF_VALUE) &&
            !image.getContentType().equals(MediaType.IMAGE_PNG_VALUE)) {

            throw new ResponseStatusException(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "Media type not supported");
        }

        try {
            logger.info("Image file name: {}", image.getName()); 
            logger.info("Image original file name: {}", image.getOriginalFilename()); 

            ImageMetadataDTO imageMetadataDTO = imageService.saveSingleImage(imageId, image.getContentType(), notes, 
                image.getBytes(), image.getOriginalFilename());

            return new ResponseEntity<>(imageMetadataDTO, HttpStatus.CREATED);

        } catch (ItemExistsException itemExistsEx) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "The image already exists");
        } catch (IOException ioEx) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ioEx.getMessage());
        }

    }


    @PostMapping(path="/image-upload-multiple/{imageId}")
    public ResponseEntity<?>  uploadMultipleImages(@RequestParam(required = false) MultipartFile imagexs,
        @RequestParam(required = false) MultipartFile imagesm, @RequestParam(required = false) MultipartFile imagemd,
        @RequestParam(required = false) MultipartFile imagelg, @RequestParam(required = false) ImageSize defaultSize,
        @PathVariable("imageId") String imageId, @RequestParam(required = false) String notes) {

        logger.info("Uploading new image {}", imageId);

        // Validate at least one image to upload was provided
        if (imagexs == null && imagesm == null && imagemd == null && imagelg == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No image to upload");
        }

        // Validate that the image is within the maximum size
        if (imagexs != null && imagexs.getSize() > MAX_IMAGE_UPLOAD_SIZE) {
            throw new ResponseStatusException(HttpStatus.PAYLOAD_TOO_LARGE, "XS image too large");
        }

        if (imagemd != null && imagesm.getSize() > MAX_IMAGE_UPLOAD_SIZE) {
            throw new ResponseStatusException(HttpStatus.PAYLOAD_TOO_LARGE, "SM image too large");
        }

        if (imagexs != null && imagemd.getSize() > MAX_IMAGE_UPLOAD_SIZE) {
            throw new ResponseStatusException(HttpStatus.PAYLOAD_TOO_LARGE, "MD image too large");
        }

        if (imagexs != null && imagexs.getSize() > MAX_IMAGE_UPLOAD_SIZE) {
            throw new ResponseStatusException(HttpStatus.PAYLOAD_TOO_LARGE, "LG image too large");
        }

        // Validate that the image type is supported
        if (imagexs != null && !imagexs.getContentType().equals(MediaType.IMAGE_JPEG_VALUE) &&
            !imagexs.getContentType().equals(MediaType.IMAGE_GIF_VALUE) &&
            !imagexs.getContentType().equals(MediaType.IMAGE_PNG_VALUE)) {
            throw new ResponseStatusException(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "XS image media type not supported");
        }

        if (imagesm != null && !imagesm.getContentType().equals(MediaType.IMAGE_JPEG_VALUE) &&
            !imagesm.getContentType().equals(MediaType.IMAGE_GIF_VALUE) &&
            !imagesm.getContentType().equals(MediaType.IMAGE_PNG_VALUE)) {
            throw new ResponseStatusException(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "SM image media type not supported");
        }

        if (imagemd != null && !imagemd.getContentType().equals(MediaType.IMAGE_JPEG_VALUE) &&
            !imagemd.getContentType().equals(MediaType.IMAGE_GIF_VALUE) &&
            !imagemd.getContentType().equals(MediaType.IMAGE_PNG_VALUE)) {
            throw new ResponseStatusException(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "MD image media type not supported");
        }

        if (imagelg != null && !imagelg.getContentType().equals(MediaType.IMAGE_JPEG_VALUE) &&
            !imagelg.getContentType().equals(MediaType.IMAGE_GIF_VALUE) &&
            !imagelg.getContentType().equals(MediaType.IMAGE_PNG_VALUE)) {
            throw new ResponseStatusException(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "LG image media type not supported");
        }

        // Validate that all images are of the same content type
        String contentType = null;

        if (imagexs != null) {
            contentType = imagexs.getContentType();
        }

        if (imagesm != null) {
            if (contentType == null) {
                contentType = imagesm.getContentType();
            } else if (!imagesm.getContentType().equals(contentType)) {
                throw new ResponseStatusException(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "Content type for all images should be the same");                
            }
        }

        if (imagemd != null) {
            if (contentType == null) {
                contentType = imagemd.getContentType();
            } else if (!imagemd.getContentType().equals(contentType)) {
                throw new ResponseStatusException(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "Content type for all images should be the same");                
            }
        }

        if (imagelg != null) {
            if (contentType == null) {
                contentType = imagelg.getContentType();
            } else if (!imagelg.getContentType().equals(contentType)) {
                throw new ResponseStatusException(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "Content type for all images should be the same");                
            }
        }

        if (contentType == null) {
            throw new ResponseStatusException(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "Cannot determine content type of images");
        }

        try {

            if (defaultSize == null) {
                defaultSize = ImageSize.md;
            }

            ImageMetadataDTO imageMetadataDTO = imageService.saveMultiImage(imageId, contentType, defaultSize, notes, 
                (imagexs != null ? imagexs.getBytes() : null), (imagexs != null ? imagexs.getOriginalFilename() : null),
                (imagesm != null ? imagesm.getBytes() : null), (imagesm != null ? imagesm.getOriginalFilename() : null),
                (imagemd != null ? imagemd.getBytes() : null), (imagemd != null ? imagemd.getOriginalFilename() : null),
                (imagelg != null ? imagelg.getBytes() : null), (imagelg != null ? imagelg.getOriginalFilename() : null)
                );

            return new ResponseEntity<>(imageMetadataDTO, HttpStatus.CREATED);

        } catch (ItemExistsException itemExistsEx) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "The image already exists");
        } catch (IOException ioEx) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ioEx.getMessage());
        }

    }    
}

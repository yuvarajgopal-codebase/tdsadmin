package com.tdsecurities.tdsadmin.controller;

import javax.validation.Validator;

import com.tdsecurities.common.dto.WorkflowEventDTO;
import com.tdsecurities.common.exception.WorkflowException;
import com.tdsecurities.common.service.PageService;
import com.tdsecurities.common.service.WorkflowService;
import com.tdsecurities.common.validation.IdentifierConstraint;
import com.tdsecurities.tdsadmin.dto.PageSubmissionDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

@Controller
@RequestMapping(path="/api/v1")
@Validated
public class PageSubmissionController {

    private static final Logger logger = LoggerFactory.getLogger(PageSubmissionController.class);

    @Autowired
    Validator validator;

    @Autowired
    PageService pageService;
    
    @Autowired
    WorkflowService workflowService;

    @PostMapping(path="/content/{pageId}/submit-for-approval", consumes =  "application/json", produces = "application/json")
    public ResponseEntity<?> submitPageForApproval(@PathVariable("pageId") @IdentifierConstraint String pageId, 
        @RequestBody PageSubmissionDTO pageSubmissionDTO) {

        logger.info("Processing request to submit page {} for approval {}", pageId);

        try {
            boolean pageExists = pageService.pageExists(pageId);

            if (!pageExists) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The page to submit for approval was not found");
            }

            String submissionMessage = "Submitting page " + pageId + " for approval";

            if (pageSubmissionDTO != null && pageSubmissionDTO.getSubmissionMessage() != null) {
                submissionMessage = pageSubmissionDTO.getSubmissionMessage();
            }

            WorkflowEventDTO workflowEvent = workflowService.submitPageForApproval(pageId, submissionMessage);
            return new ResponseEntity<>(workflowEvent, HttpStatus.OK);

        } catch (WorkflowException workflowEx) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, workflowEx.getMessage());        
        } 
    }

}


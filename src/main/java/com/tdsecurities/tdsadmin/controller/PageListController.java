package com.tdsecurities.tdsadmin.controller;

import java.util.List;

import com.tdsecurities.common.dto.PageInfoDTO;
import com.tdsecurities.common.service.PageService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path="/api/v1")
public class PageListController {

    private static final Logger logger = LoggerFactory.getLogger(PageListController.class);

    @Autowired
    PageService pageService;

    @GetMapping(path="/content/pages")
    public ResponseEntity<?> getPages() {

        logger.info("Processing request to get all pages");

        List<PageInfoDTO> pageInfoList = pageService.getPages();

        logger.info("Found {} pages", (pageInfoList != null ? pageInfoList.size(): 0));

        return new ResponseEntity<>(pageInfoList, HttpStatus.OK);
    }
    
}
package com.tdsecurities.tdsadmin.controller;

import java.util.List;

import com.tdsecurities.common.dto.TemplateInfoDTO;
import com.tdsecurities.common.service.TemplateService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path="/api/v1")
public class TemplateListController {

    private static final Logger logger = LoggerFactory.getLogger(PageListController.class);

    @Autowired
    TemplateService templateService;

    @GetMapping(path="/templates")
    public ResponseEntity<?> getTemplates() {

        logger.info("Processing request to get all templates");

        List<TemplateInfoDTO> templateInfoList = templateService.getTemplates();

        logger.info("Found {} templates", (templateInfoList != null ? templateInfoList.size(): 0));

        return new ResponseEntity<>(templateInfoList, HttpStatus.OK);

    }
    
}
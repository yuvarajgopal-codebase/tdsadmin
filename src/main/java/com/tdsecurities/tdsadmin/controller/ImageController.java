package com.tdsecurities.tdsadmin.controller;

import java.util.List;

import com.tdsecurities.common.data.ImageSize;
import com.tdsecurities.common.dto.ImageDTO;
import com.tdsecurities.common.dto.ImageMetadataDTO;
import com.tdsecurities.common.exception.ItemNotFoundException;
import com.tdsecurities.common.service.ImageService;
import com.tdsecurities.common.util.ImageUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;


@Controller
@RequestMapping(path="/api/v1")
public class ImageController {

    private static final Logger logger = LoggerFactory.getLogger(ImageController.class);

    @Autowired
    ImageService imageService;

    @GetMapping(path = "/image/{imageId}/{version}")
    public ResponseEntity<byte[]> getImage(@PathVariable("imageId") String imageId, @PathVariable("version") long version, 
        @RequestParam(required = false) ImageSize size) {

        logger.info("Requesting image {}", imageId);

        try {
            ImageDTO imageDTO = null;
            
            if (size != null) {
                imageDTO = imageService.getImage(imageId, version, size);
            } else {
                imageDTO = imageService.getDefaultImage(imageId, version);
            }

            if (imageDTO == null) {
                logger.warn("The image {} was not found", imageId);
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The image was not found");
            }

            if (imageDTO.getImageData() == null) {
                logger.warn("No data for image {} was found", imageId);
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The image was not found");
            }

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(ImageUtils.getMediaTypeForMimeType(imageDTO.getMimeType()));

            ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(imageDTO.getImageData(), headers, HttpStatus.OK);
            return responseEntity;

        } catch (ItemNotFoundException itemNotFoundEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The image was not found");
        } 
    }

    @GetMapping(path="/image-metadata")
    public ResponseEntity<?> getImageMetadata() {

        logger.info("Processing request to metadata for all images");

        try {
            List<ImageMetadataDTO> imageMetadataList = imageService.getImageMetadataForAllImages();
            
            logger.info("Found {} images", (imageMetadataList != null ? imageMetadataList.size(): 0));

            return new ResponseEntity<>(imageMetadataList, HttpStatus.OK);

        } catch (ItemNotFoundException itemNotFoundEx) {

            logger.warn("No image metadata was found: {}", itemNotFoundEx.getMessage());
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Image metadata was not found");
        }

    }

}
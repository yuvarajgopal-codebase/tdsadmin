package com.tdsecurities.tdsadmin.controller;

import com.tdsecurities.common.dto.PageInfoDTO;
import com.tdsecurities.common.exception.PageExistsException;
import com.tdsecurities.common.exception.TemplateNotFoundException;
import com.tdsecurities.common.service.PageService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

@Controller
@RequestMapping(path="/api/v1")
public class PageCreateController {

    private static final Logger logger = LoggerFactory.getLogger(PageEditController.class);

    @Autowired
    PageService pageService;
    
    @PostMapping(path="/content/{pageId}", consumes =  "application/json", produces = "application/json")
    public ResponseEntity<?> createPage(@PathVariable("pageId") String pageId,
        @RequestBody PageInfoDTO pageInfoDTO) {

        logger.info("Processing request to create page {}", pageId);

        try {

            PageInfoDTO createdPage = pageService.createPage(pageId, pageInfoDTO.getTemplateId(), pageInfoDTO.getNotes());
            return new ResponseEntity<>(createdPage, HttpStatus.CREATED);
    
        } catch (PageExistsException pageExistsEx) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "The page already exists");
        } catch (TemplateNotFoundException templateNotFoundEx){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The template specified is invalid");
        }

    }
}
package com.tdsecurities.tdsadmin.controller;

import com.tdsecurities.common.dto.PageEditDTO;
import com.tdsecurities.common.dto.PageUpdateDTO;
import com.tdsecurities.common.exception.PageDataParsingException;
import com.tdsecurities.common.exception.PageNotEditableException;
import com.tdsecurities.common.exception.PageNotFoundException;
import com.tdsecurities.common.service.PageEditService;
import com.tdsecurities.common.support.StandardResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

@Controller
@RequestMapping(path="/api/v1")
public class PageEditController {

    private static final Logger logger = LoggerFactory.getLogger(PageEditController.class);

    @Autowired
    PageEditService pageEditService;

    private String DEFAULT_LANG = "en_CA";

    @GetMapping(path="/content/{pageId}/edit")
    public ResponseEntity<?> getPageDataToEdit(@PathVariable("pageId") String pageId, 
        @RequestParam(required = false) String language) {

        logger.info("Processing request to edit page {} with language {}", 
            pageId, language);

        if (language == null) {
            language = DEFAULT_LANG;
        }

        try {
            PageEditDTO pageEditDTO = pageEditService.getPageToEdit(pageId, language);
            return new ResponseEntity<>(pageEditDTO, HttpStatus.OK);

        } catch (PageNotFoundException pageNotFoundException) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The page to edit was not found");
        } catch (PageNotEditableException pageDataNotEditableException) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, pageDataNotEditableException.getMessage());
        }
    }

    @PostMapping(path="/content/{pageId}/edit", consumes =  "application/json", produces = "application/json")
    public ResponseEntity<?> updatePageDataToEdit(@PathVariable("pageId") String pageId, 
        @RequestParam(required = false) String language, @RequestBody PageUpdateDTO pageUpdateDTO) {

        logger.info("Processing request to updata page {} with language {}", pageId, language);

        if (language == null) {
            language = DEFAULT_LANG;
        }

        try {
            pageEditService.updateEditedPage(pageId, language, pageUpdateDTO);
            return new ResponseEntity<>(new StandardResponse(), HttpStatus.OK);
        } catch (PageNotFoundException pageNotFoundEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The page to update was not found");
        } catch (PageDataParsingException pageDataParsingEx) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The page data to update was invalid");        
        } catch (PageNotEditableException pageDataNotEditableException) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, pageDataNotEditableException.getMessage());
        }
    }
}
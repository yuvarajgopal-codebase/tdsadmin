package com.tdsecurities.tdsadmin;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.PathResourceResolver;

/**
 * Used to run application as a SpringBoot war.
 * Uncomment annotations to use.
 */

@SpringBootApplication(scanBasePackages={"com.tdsecurities.tdsadmin", "com.tdsecurities.common"})
public class TDSAdminApplication extends SpringBootServletInitializer  {

    private static final Logger logger = LoggerFactory.getLogger(TDSAdminApplication.class);

    public static void main(String args[]) {
        SpringApplication.run(TDSAdminApplication.class, args);
        logger.info("Started TD Securities Content Admin Application");
        System.out.println("Started TD Securities Content Admin Application");
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(TDSAdminApplication.class);
    }

    /* TODO: Fix after dev. Cors is temporary Angular testing. Resource handler is permanent for Angular  */
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {

            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/api/v1/**").allowedOrigins("http://localhost:4200");
            }

            @Override
            public void addResourceHandlers(ResourceHandlerRegistry registry) {
                registry.addResourceHandler("/**/*")
                .addResourceLocations("classpath:/resources/")
                .resourceChain(true)
                .addResolver(new PathResourceResolver() {
                    @Override
                    protected Resource getResource(String resourcePath,
                        Resource location) throws IOException {
                        Resource requestedResource = location.createRelative(resourcePath);
                        return requestedResource.exists() && requestedResource.isReadable() ? requestedResource
                        : new ClassPathResource("/resources/index.html");
                    }
                });
            }
        };
    }
}
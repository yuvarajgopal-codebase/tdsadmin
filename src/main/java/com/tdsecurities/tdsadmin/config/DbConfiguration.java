package com.tdsecurities.tdsadmin.config;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jndi.JndiTemplate;
import org.springframework.web.client.RestTemplate;

@Configuration
public class DbConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(DbConfiguration.class);

    @Autowired
    private Environment env;
    
    @Bean
    public DataSource getDataSource() throws NamingException {
        logger.info("Setting up datasource");
        boolean useAzureDatasaource = Boolean.parseBoolean(env.getProperty("tdsadmin.useAzureDatasource"));

        if (useAzureDatasaource) {
            JndiTemplate jndiTemplate = new JndiTemplate();
            DataSource azureDatasource = (DataSource)jndiTemplate.lookup(env.getProperty("tdsadmin.datasource.azure.jndi-name"));
            return azureDatasource;
        } else {
            DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
            dataSourceBuilder.driverClassName(env.getProperty("spring.datasource.driverClassName"));
            dataSourceBuilder.url(env.getProperty("tdsadmin.datasource.local.url"));
            dataSourceBuilder.username(env.getProperty("tdsadmin.datasource.local.username"));
            dataSourceBuilder.password(env.getProperty("tdsadmin.datasource.local.password"));
            return dataSourceBuilder.build();
            }

    }
    
    /*
    @Bean
    public DataSource getAzureDataSource() {
        RestTemplate restTemplate = new RestTemplate();
        String azureTokenUrl = env.getProperty("spring.datasource.azure.tokenUrl");

        HttpHeaders headers = new HttpHeaders();
        headers.set("Metadata", "true");
        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

        logger.info("About to connect to DB token service");
        ResponseEntity<String> response = restTemplate.exchange(azureTokenUrl, HttpMethod.GET, entity, String.class);

        if (response.getStatusCode() != HttpStatus.OK) {
            logger.error("Failed to create connection to Azure DB. Error: " + response.getStatusCodeValue());
            throw new RuntimeException("Failed to connect to Azure DB");
        }

        logger.info("Got response from DB token service");

        JsonParser parser = new JsonParser();
        JsonElement rootElement = parser.parse(response.getBody());
        JsonObject rootObject = rootElement.getAsJsonObject();
        JsonElement accessToken = rootObject.get("access_token");

        if (accessToken == null || accessToken.isJsonPrimitive() == false) {
            logger.error("Failed to create connection to Azure DB. Error parsing access token.");
            throw new RuntimeException("Failed to connect to Azure DB.");
        }

        String accessTokenValue = accessToken.getAsString();
        logger.info("Parsed access token");

        SQLServerDataSource dataSource = new SQLServerDataSource();
        dataSource.setServerName(env.getProperty("spring.datasource.azure.serverName"));
        dataSource.setDatabaseName(env.getProperty("spring.datasource.azure.databaseName"));
        dataSource.setAccessToken(accessTokenValue);
        dataSource.setApplicationIntent("ReadOnly");
        return dataSource;
    }
    */

}
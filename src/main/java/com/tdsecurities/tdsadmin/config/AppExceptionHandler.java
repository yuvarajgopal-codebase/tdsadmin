package com.tdsecurities.tdsadmin.config;

import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.NoHandlerFoundException;

@RestControllerAdvice
public class AppExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(AppExceptionHandler.class);

    private class RestException {

        private HttpStatus errorCode;
        private String message;

        public RestException(HttpStatus errorCode, String message) {
            this.errorCode = errorCode;
            this.message = message;
        }

        public HttpStatus getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(HttpStatus errorCode) {
            this.errorCode = errorCode;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<?> handleException(Exception exception) {
        logger.error("Captured controller exception", exception);

        RestException restException = null;

        if (exception instanceof ResponseStatusException) {
            ResponseStatusException responseStatusException = (ResponseStatusException) exception;
            restException = new RestException(responseStatusException.getStatus(), responseStatusException.getReason());
        } else {
            restException = new RestException(HttpStatus.INTERNAL_SERVER_ERROR, exception.getMessage());
        }
        return new ResponseEntity<>(restException, restException.getErrorCode());
    }

    @ResponseBody
    @ExceptionHandler(value = NoHandlerFoundException.class)
    public ResponseEntity<?> handleNoHandlerFoundException(Exception exception) {
        logger.error("No controller found for path: ", exception);

        RestException restException = new RestException(HttpStatus.NOT_FOUND, exception.getMessage());
        return new ResponseEntity<>(restException, HttpStatus.NOT_FOUND);
    }

    @ResponseBody
    @ExceptionHandler(value = {ConstraintViolationException.class, MethodArgumentNotValidException.class})
    public ResponseEntity<?> handlemethodArgumentNotValidException(Exception exception) {
        logger.warn("The argument to a method was invalid: ", exception);

        RestException restException = new RestException(HttpStatus.BAD_REQUEST, exception.getMessage());
        return new ResponseEntity<>(restException, HttpStatus.BAD_REQUEST);
    }

}
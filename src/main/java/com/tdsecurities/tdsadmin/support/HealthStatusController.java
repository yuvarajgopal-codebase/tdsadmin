package com.tdsecurities.tdsadmin.support;

import java.util.Optional;

import com.tdsecurities.common.data.TemplateInfo;
import com.tdsecurities.common.data.TemplateInfoId;
import com.tdsecurities.common.data.TemplateInfoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HealthStatusController {
 
    @Autowired
    TemplateInfoRepository templateInfoRepository;

    @GetMapping(path="/ping")
    public ResponseEntity<?> performHealthCheck() {
        return new ResponseEntity<>("success", HttpStatus.OK);
    }

    @GetMapping(path="/db-ping")
    public ResponseEntity<?> performDbHealthCheck() {
        try {
            Optional<TemplateInfo> templateInfoOptional = templateInfoRepository.findById(
                new TemplateInfoId("XXYYZZ", 1));
            return new ResponseEntity<>("db success", HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>("db failure", HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

}
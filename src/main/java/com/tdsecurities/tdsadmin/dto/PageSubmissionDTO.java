package com.tdsecurities.tdsadmin.dto;

public class PageSubmissionDTO {

    private String submissionMessage;

    public String getSubmissionMessage() {
        return submissionMessage;
    }

    public void setSubmissionMessage(String submissionMessage) {
        this.submissionMessage = submissionMessage;
    }    
}
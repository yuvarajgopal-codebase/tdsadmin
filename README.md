## tdsadmin
TD Securities Public Website - Content Management Application

The TDS admin application to manage the content for the TDS public website. This includes a Spring application for the backend and an Angular application for the frontend. The Angular application will be packaged in the war file of the Spring applicaiton.

### Build
The tdsadmin web application includes an Angular application that is the CMS website.
The process for building the application is as follows:

1. Angular application:

    The built version of the tdsadmin Angular application (in the dist directory of the Angular project) should be checked in to the following directory so that it will be picked up by the Spring application build:

        /tdsadmin-angular


2. Build the Spring web application:

    From /tdsadmin-server/src/main/web, run:

        mvn clean install

    One of the steps in this build process will copy over generated Angular application and include it in the war file


NOTE: This application depends on the com.tdsecurities.tdscommon component. The proper version of the tdscommon component should be specified. If the desired version is not in the Nexus repo, then clone it and build it so that it is in your local maven cache.

### Run
NOTE: Due to deployment requirements for DataDog log file collection on Azure, the logging framework is configured to log to the following directory:

    D:\home\LogFiles\Application

The application may not startup if it can't find that log directory. If you are running locally and you don't have a D: drive, you can mimic one by performing the following:

1. Create the directory C:\D_Drive\home\LogFiles\Application
    
2. Run the command: subst d: C:\D_Drive


### Maven Repo
The TD Nexus repo should be used as the main repository. This can be configured in your Maven settings.xml file. It is also configured in the build pom.xml file. The following is the sample configuration.

```
    <mirrors>
        <mirror>
            <id>td-nexus</id>
            <name>Public</name>
            <url>https://repo.td.com/repository/maven-public/</url>
            <mirrorOf>central</mirrorOf>
        </mirror>
    </mirrors>
```

In addition, the following repositories were added to the pom.xml file so that the tdscommon component can be referenced as a dependency.

```
    <repository>
        <id>tds-maven-releases</id>
        <url>https://repo.td.com/repository/tds-maven-releases</url>
    </repository>
    <repository>
        <id>tds-maven-snapshots</id>
        <url>https://repo.td.com/repository/tds-maven-snapshots</url>
    </repository>
```
